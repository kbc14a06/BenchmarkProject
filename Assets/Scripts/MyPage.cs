﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
/*
 *  /Scripts/Json
 */
using PlayerInfo;

public class MyPage : MonoBehaviour {
    /* GameObject */
	private GameObject config;
	private Slider _sliderHp,_sliderAt,_sliderDf,_sliderAg;

    /* Status Property*/
    private Player p = new Player();
    private int maxValue, guageSpeed, _hp, _at, _df, _ag;

    
    // initialize property
    void init()
    {
        maxValue = 1000;
        guageSpeed = maxValue / 20;
        _sliderHp.maxValue = maxValue;
        _sliderAt.maxValue = maxValue;
        _sliderDf.maxValue = maxValue;
        _sliderAg.maxValue = maxValue;
    }


    // Use this for initialization
    void Start() {
		config = GameObject.Find ("Config");
		config.SetActiveRecursively (false);


        // GetComponent
		_sliderHp= GameObject.Find("HpSlider").GetComponent<Slider>();
		_sliderAt = GameObject.Find("AttackSlider").GetComponent<Slider>();
		_sliderDf = GameObject.Find("DefenceSlider").GetComponent<Slider>();
		_sliderAg = GameObject.Find("AgilitySlider").GetComponent<Slider>();

        // initialize
        init();

        // test
        p.hp = 531;
        p.at = 552;
        p.df = 823;
        p.ag = 486;
    }


	// Update is called once per frame
	void Update () {
        // Conditional branch
        if( _hp < p.hp )
        {
            _hp += guageSpeed;
        }
        if( _at < p.at )
        {
            _at += guageSpeed;
        }
        if( _df < p.df )
        {
            _df += guageSpeed;
        }
        if( _ag < p.ag )
        {
            _ag += guageSpeed;
        }

        // set to property
		_sliderHp.value = _hp;
		_sliderAt.value = _at;
		_sliderDf.value = _df;
		_sliderAg.value = _ag;

	}


	public void onConfigButton(){
		config.SetActiveRecursively (true);
	}

	public void delConfig(){
		config.SetActiveRecursively (false);
	}
}
