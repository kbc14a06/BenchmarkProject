﻿using UnityEngine;
using UnityEngine.UI;
using SocketIO;

/* 
 * /Scripts/Json 
 */
using PlayerInfo;

public class Login : MonoBehaviour
{
    // Socket
    private static GameObject go;
    private static SocketIOComponent socket;

    // Data
    private Player data;
    JSONObject json;

    // GameObject
    private new string name;
    private string mail;
    private string pass;
    public InputField inputField1;
    public InputField inputField2;
    public Text text1;
    public Text text2;



    // onClick
    public void start()
    {
        name = inputField1.text;
        pass = inputField2.text;
        mail = "kbc14a06@stu.kawahara.ac.jp"; // テストの値
        
        if (name == "" | pass == "") return; // 入力されていない場合はreturn

        
        // Playerの生成
        Player.Instance.name = name;
        Player.Instance.mail = mail;
        Player.Instance.pass = pass;
        Player.Instance.Save(); // セーブ（テスト）

        string str = LitJson.JsonMapper.ToJson(Player.Instance); // Json(String型)にシリアライズ
        json = new JSONObject(str); // JSONObjectにキャスト

        //Debug.Log(json);

        // 送信
        go = GameObject.Find("SocketIO");
        socket = go.GetComponent<SocketIOComponent>();
        emit(json);
        
        Debug.Log(PlayerPrefs.GetString("Player"));
        
    }

    // 送信処理
    public void emit(JSONObject json)
    {
        socket.Emit("test", json);
    }


    // 受信処理
    public void get()
    {
        socket.On("get", getObjectScheme);
    }


    // 受信
    public void getObjectScheme(SocketIOEvent e)
    {
        Debug.Log(e.name + e.data);
    }


}

