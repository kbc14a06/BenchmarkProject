﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;


public class test : MonoBehaviour {
    private SocketIOComponent socket;
    private string msg;

    public void getObject(SocketIOEvent e)
    {
        this.GetComponent<Text>().text = "" + e.name + e.data;
    }

    // Update is called once per frame
    void Update () {
        GameObject go = GameObject.Find("SocketIO");
        socket = go.GetComponent<SocketIOComponent>();

        msg = "{\"name\":\"horikawa show\"}";
        JSONObject init = new JSONObject(msg);

        socket.Emit("test", init);
        socket.On("get", getObject);
    }
}
