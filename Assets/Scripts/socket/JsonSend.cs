﻿using UnityEngine;
using System.Collections;
using SocketIO;

public class JsonSend : MonoBehaviour {
    private SocketIOComponent socket;
    private GameObject go = GameObject.Find("SocketIO");

    public void emit(string target, string msg)
    {
        socket = go.GetComponent<SocketIOComponent>();
        JSONObject json = new JSONObject(msg);
        socket.Emit(target, json);
    }

    public void emit(string target, JSONObject json)
    {
        socket = go.GetComponent<SocketIOComponent>();
        socket.Emit(target, json);
    }
}
