﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using SocketIO;

public class JsonRecieve : MonoBehaviour
{
    private SocketIOComponent socket;
    private GameObject go = GameObject.Find("SocketIO");
    private SocketIOEvent _event;

    // GET
    public SocketIOEvent on(string target)
    {
        socket = go.GetComponent<SocketIOComponent>();
        socket.On(target, (SocketIOEvent e) => { _event = e; });
        return _event;
    }

}