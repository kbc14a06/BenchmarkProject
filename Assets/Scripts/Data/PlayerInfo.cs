﻿using UnityEngine;
using UnityEngine.UI;

/*
 *  /Scripts/Json
 */
using SaveDataManager;
using LitJson;

namespace PlayerInfo
{
    /* Player Class */
    public class Player : SavableSingleton<Player>
    {
        /* Player info */
        private int id { get; set; }
        public string name { get; set; }
        public string mail { get; set; }
        public string pass { get; set; }

        /* Status */
        public int hp { get; set; }
        public int at { get; set; }
        public int df { get; set; }
        public int ag { get; set; }
    }

}
