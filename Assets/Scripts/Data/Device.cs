﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LitJson;

/* Device Class */

public class Device : MonoBehaviour
{
    public DeviceData deviceData;
    private JsonData jsonData;
    private string brand;

    // Constructer
    void Start()
    {
        
        AndroidJavaObject plugin = new AndroidJavaObject("com.example.kbc14a05.lib.DeviceInfo");
        AndroidJavaObject deviceInfo = plugin.CallStatic<AndroidJavaObject>("getInstance");
        string jsonText = deviceInfo.Call<string>("getDeviceInfo");
        jsonData = JsonMapper.ToObject(jsonText);


        deviceData.brand = (string)jsonData["basicInfo"]["brand"];

    }


}