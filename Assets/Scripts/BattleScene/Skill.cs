﻿using UnityEngine;
using System.Collections;


public enum SkillType{
	ATTACK,
	SKILL,
	DEFENSE,
	HEAL
}

[System.Serializable]
public class Skill{

	public int skillNo;
	public string skillName;
	public string skill;
	public string skilltype;

	public Skill(int sNo,string sName,string sk,string st){
		skillNo=sNo;
		skillName=sName;
		skill=sk;
		skilltype = st;
	}

	/*
	public int getSkillNo(){
		return skillNo;
	}

	public string getSkillName(){
		return skillName;
	}

	public string getSkill(){
		return skill;
	}
	*/



	/*
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	*/
}
