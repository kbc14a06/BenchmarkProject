﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AtSkillList : MonoBehaviour {

	public List<Skill> skillList = new List<Skill>();

	// Use this for initialization
	void Start () {

		//テスト用データ
		//{スキルNo,スキル名,効果,種類(ATTACK,SKILL,DEFENSE,HEAL)}
		skillList.Add(new Skill(1,"攻撃","相手にダメージを与える","ATTACK"));
		skillList.Add(new Skill(2,"強い攻撃","相手に強めのダメージを与える","ATTACK"));
		skillList.Add(new Skill(3,"魔法攻撃","魔法の力で攻撃する","ATTACK"));
		skillList.Add(new Skill(4,"斬撃","剣を振る","ATTACK"));
		skillList.Add(new Skill(5,"必殺技","相手は死ぬ","ATTACK"));
		skillList.Add(new Skill(6,"すごいやつ","とてつもなくすごい","ATTACK"));
		skillList.Add(new Skill(7,"回復","HPが少し回復する","HEAL"));
		skillList.Add(new Skill(8,"俊敏","素早さがアップする","SKILL"));
		skillList.Add(new Skill(9,"気合い","攻撃力がアップする","SKILL"));
		skillList.Add(new Skill(10,"回復(大)","HPがたくさん回復する代わりに防御が下がる","HEAL"));
		skillList.Add(new Skill(11,"防御","守りの体制に入る","DEFENSE"));
		skillList.Add(new Skill(12,"すごい防御","すごい","DEFENSE"));
		skillList.Add(new Skill(13,"無効化","相手のスキルを一回無効化する","SKILL"));
	
	}
}
