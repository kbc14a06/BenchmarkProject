﻿/*HPを管理する
 * 
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class playerManager : MonoBehaviour {
	int maxHP; //最大ＨＰ
	int damage; //ダメージ
	int Hp; //今のＨＰ
	public Slider hpBar; //スライダー
	public Text maxHpText; //HPを表す数字(Hp/MaxHP)
	public Text playerName; //プレイヤーの名前
	public BattlePlayer bp;

	// Use this for initialization
	void Start () {

		maxHP=bp.Hp; //テスト用にてきとう
		Hp=maxHP;
		hpBar.maxValue=maxHP;
		maxHpText.text = maxHP + "/" + maxHP;
		hpBar.value = Hp;

		playerName.text = bp.playerName; //テスト用にてきとう
	}
	
	// Update is called once per frame
	void Update () {

		hpBar.value=Hp;
	
	}
}
