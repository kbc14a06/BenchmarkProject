﻿/*コマンド選択画面
 * スキルをSkillListに格納していると表示する
 * attackボタンで攻撃すると、選ばれたスキル番号を送る（予定）
*/


using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Linq;

public class Command : MonoBehaviour {

	private GameObject ss; //SimpleSelect
	public ToggleGroup toggleGroup;
	private string selectedValue; //４種類からどのコマンドを選んだかを入れる
	public RectTransform prefab = null; //プレハブ
	private GameObject con; //Content
	private int Count=0;
	public AtSkillList skillList; //スキルが入ったリスト
	public Text selectSkillName,selectSkill,selectSkillNo; //選択されたコマンドを表示する領域
	private GameObject select1, select2, select3, select4; //選択されたボタンの色を変える

	// Use this for initialization
	void Start () {
		ss = GameObject.Find ("SimpleSelect");
		ss.SetActiveRecursively (false);

		selectSkillName.text = "";
		selectSkill.text="コマンドを選択してください";

		select1 = GameObject.Find ("Toggle1-select");
		select1.SetActiveRecursively (false);
		select2 = GameObject.Find ("Toggle2-select");
		select2.SetActiveRecursively (false);
		select3 = GameObject.Find ("Toggle3-select");
		select3.SetActiveRecursively (false);
		select4 = GameObject.Find ("Toggle4-select");
		select4.SetActiveRecursively (false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void onAttack() {
		//Attackボタンを押したとき
		if (selectSkillNo.text == "-1") {
			//何もコマンドが選択されていなかった場合
			//コマンドを選ばせる
			Debug.Log ("何も選択してないよ！");
		} else {
			//コマンドが選択されていた場合
			//攻撃を行う
			Debug.Log ("攻撃するよ！使うスキルはこれ！スキル番号：" + selectSkillNo.text);
		}
	}

	public void onSimpleButton(){
		//4種類のコマンドから選択したものの処理
		ss.SetActiveRecursively (true);

		try{
			selectedValue = toggleGroup.ActiveToggles ()
				.FirstOrDefault ().GetComponentsInChildren<Text> ()
				.First (t => t.name == "Value").text;

			//Debug.Log ("selected " + selectedValue);
		}catch (NullReferenceException ex){
			Debug.Log ("選択してください");
		}

		int val = int.Parse(selectedValue);

		con= GameObject.Find("Content") as GameObject;

		//なぜか２回呼び出されてしまうので２回目は避ける
		if (Count < 1) {
			
			switch (val) {
			case 1:
				//攻撃
				//Debug.Log ("selected 1");
				List ("ATTACK",skillList);
				select1.SetActiveRecursively (true);
				select2.SetActiveRecursively (false);
				select3.SetActiveRecursively (false);
				select4.SetActiveRecursively (false);
				break;
			case 2:
				//スキル
				//Debug.Log ("selected 2");
				List ("SKILL",skillList);
				select1.SetActiveRecursively (false);
				select2.SetActiveRecursively (true);
				select3.SetActiveRecursively (false);
				select4.SetActiveRecursively (false);
				break;
			case 3:
				//防御
				//Debug.Log ("selected 3");
				List ("DEFENSE",skillList);
				select1.SetActiveRecursively (false);
				select2.SetActiveRecursively (false);
				select3.SetActiveRecursively (true);
				select4.SetActiveRecursively (false);
				break;
			case 4:
				//回復
				//Debug.Log ("selected 4");
				List ("HEAL",skillList);
				select1.SetActiveRecursively (false);
				select2.SetActiveRecursively (false);
				select3.SetActiveRecursively (false);
				select4.SetActiveRecursively (true);
				break;
			default:
				Debug.Log ("selected 0");
				break;
			}
			//Debug.Log ("終了");
		}
	}

	public void selectAt(string name,string content,string no){
		//リストを削除
		foreach ( Transform n in con.transform )
		{
			GameObject.Destroy(n.gameObject);
		}
			

		selectSkillName.text = name;
		selectSkill.text = content;
		selectSkillNo.text = no;
		Count = 0;
		ss.SetActiveRecursively (false);
	}


	public void List(String type,AtSkillList atskillList){
		
		//LINQを使ってスキルを種類で絞り込み
		var selectlist = from skill in atskillList.skillList
		                 where skill.skilltype.Contains (type)
		                 select skill;

		foreach (var sl in selectlist)
		{
			//プレハブを作る
			RectTransform item = GameObject.Instantiate (prefab) as RectTransform;
			//ローカル座標を維持したまま親オブジェクトを設定
			item.SetParent (transform, false);
			//親はContentに指定
			item.transform.parent = con.transform;
			//itemの大きさ調整
			item.transform.localScale = new Vector3 (1, 1, 1);

			//スキル名をtextに入れる
			String skillName = sl.skillName;
			var text = item.FindChild ("Button").FindChild ("SkillName").GetComponentInChildren<Text> ();
			text.text = skillName;

			//スキル詳細をtextに入れる
			String skill = sl.skill;
			text = item.FindChild ("Button").FindChild ("Skill").GetComponentInChildren<Text> ();
			text.text = skill;

			//スキルナンバーをtextにいれる
			string skillNo = sl.skillNo.ToString();
			text = item.FindChild ("Button").FindChild ("SkillNo").GetComponentInChildren<Text> ();
			text.text = skillNo;

			item.GetComponentInChildren<Button> ().onClick.AddListener (() => selectAt (skillName,skill,skillNo));


			Count++;

		} 
	}
		
		
}
