﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AttachText : MonoBehaviour {
    public DeviceData deviceData;
    public Text _brand;

	// Use this for initialization
	void Start () {
        _brand.text = deviceData.brand;
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
