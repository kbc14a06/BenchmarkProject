"use strict";
/// <reference path="model.js"/>
var mongo = require("./model");
var Schema = require("./schema/SwitchSchema");
var schema = new Schema.SwitchSchema();
var models = {
    player: mongo('player', schema.getSchema('Player'))
};
var useDB = (function () {
    function useDB() {
    }
    useDB.prototype.find = function (collectionName, search, callback) {
        models[collectionName].find(search).then(callback);
    };
    useDB.prototype.findOne = function (collectionName, search, callback) {
        models[collectionName].findOne(search).then(callback);
    };
    //特殊なことしたいときはこっち
    useDB.prototype.update = function (collectionName, where, values, option) {
        models[collectionName].update(values, where, option);
    };
    //普通に登録、更新したいときはこっち
    useDB.prototype.upsert = function (collectionName, where, values) {
        models[collectionName].update(values, where, { upsert: true });
    };
    useDB.prototype.saveNotExists = function (collectionName, where, values) {
        values = { $setOnInsert: values };
        models[collectionName].update(values, where, { upsert: true });
    };
    //初回登録時のみの実行が望ましい
    useDB.prototype.save = function (collectionName, datas, callback) {
        var ins = models[collectionName].getInstance();
        for (var name in datas) {
            ins[name] = datas[name];
        }
        ins.save(callback);
    };
    useDB.prototype.connect = function () {
        models['player'].connection();
    };
    useDB.prototype.close = function () {
        models['player'].close();
    };
    return useDB;
}());
exports.useDB = useDB;
//# sourceMappingURL=WrapModel.js.map