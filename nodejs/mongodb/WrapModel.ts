/// <reference path="model.js"/>
import * as mongo from "./model"
import * as Schema from "./schema/SwitchSchema"

const schema = new Schema.SwitchSchema();
const models = {
	player:mongo('player',schema.getSchema('Player'))
}
export class useDB {

	find(collectionName,search, callback) {
		models[collectionName].find(search).then(callback);
	}

	findOne(collectionName,search,callback) {
		models[collectionName].findOne(search).then(callback)
	}
	
	//特殊なことしたいときはこっち
	update(collectionName,where,values,option) {
		models[collectionName].update(values,where,option);
	}
	
	//普通に登録、更新したいときはこっち
	upsert(collectionName,where,values) {
		models[collectionName].update(values,where,{upsert:true});
	}

	saveNotExists(collectionName,where,values) {
		values = {$setOnInsert:values}
		models[collectionName].update(values,where,{upsert:true});
	}

	//初回登録時のみの実行が望ましい
	save(collectionName,datas, callback) {
		var ins = models[collectionName].getInstance();
		for (var name in datas) {
			ins[name] = datas[name]
		}
		ins.save(callback);
	}

	connect() {
		models['player'].connection();
	}

	close() {
		models['player'].close();
	}
}
