/// <reference path="Schema.ts"/>
import {Schema} from "./Schema";

export class SkillSchema implements Schema {

	/**
	 * name スキル名
	 * skilleffects スキル効果など以下例
	 *              {skilltype:0,  (攻撃:0,防御:1,回復2)
	 *               power:100,    (威力)
	 *               effecttype:1, (0:なし,1:毒)
	 *               effect:100}   (状態異常時のダメージ倍率的な奴)
	 * @type {{name: StringConstructor, skilleffects: ObjectConstructor}}
	 */
	private schema = {
		name: String,
		skilleffects: Object
	}

	getSchema():Object {
		return this.schema;
	}
}