"use strict";
/// <reference path="PlayerSchema" />
/// <reference path="SkillSchema" />
var PlayerSchema_1 = require("./PlayerSchema");
var SkillSchema_1 = require("./SkillSchema");
var Schema = {
    'Player': new PlayerSchema_1.PlayerSchema(),
    'Skill': new SkillSchema_1.SkillSchema()
};
var SwitchSchema = (function () {
    function SwitchSchema() {
    }
    SwitchSchema.prototype.getSchema = function (type) {
        return Schema[type].getSchema();
    };
    return SwitchSchema;
}());
exports.SwitchSchema = SwitchSchema;
//# sourceMappingURL=SwitchSchema.js.map