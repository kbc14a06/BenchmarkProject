/// <reference path="Schema.ts"/>
import {Schema} from "./Schema";

export class PlayerSchema implements Schema {
	private schema = {
		'name': String,
		'email': String,
		'pass':String,
		'level': Number,
		'hp': Number,
		'atk': Number,
		'def': Number,
		'agi': Number,
		'exp': Number
	};

	getSchema():Object {
		return this.schema
	}
}