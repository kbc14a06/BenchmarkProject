"use strict";
var PlayerSchema = (function () {
    function PlayerSchema() {
        this.schema = {
            'name': String,
            'email': String,
            'pass': String,
            'level': Number,
            'hp': Number,
            'atk': Number,
            'def': Number,
            'agi': Number,
            'exp': Number
        };
    }
    PlayerSchema.prototype.getSchema = function () {
        return this.schema;
    };
    return PlayerSchema;
}());
exports.PlayerSchema = PlayerSchema;
//# sourceMappingURL=PlayerSchema.js.map