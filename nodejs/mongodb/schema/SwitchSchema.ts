/// <reference path="PlayerSchema" />
/// <reference path="SkillSchema" />
import {PlayerSchema} from "./PlayerSchema";
import {SkillSchema} from  "./SkillSchema";

const Schema = {
	'Player': new PlayerSchema(),
	'Skill': new SkillSchema()
	
}
export class SwitchSchema {
	getSchema(type: string):Object {
		return Schema[type].getSchema()
	}
}