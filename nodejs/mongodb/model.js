/**
 * Created by bochan773 on 2016/06/13.
 */

var mongoose = require('mongoose'),
	Schema = mongoose.Schema;

/*---------------------------------------------
 * ModelModule定義
 *--------------------------------------------*/
/**
 * コンストラクタ
 * @document : ドキュメント名
 * @field    : フィールド定義
 */
var ModelModule = function () {
};

/**
 * スキーマ作成
 */
ModelModule.prototype.create = function () {
	var ModelSchema = new Schema(this.field, {versionKey: false});
	mongoose.model(this.document, ModelSchema);
	this.scheme = mongoose.model(this.document);
	console.log('create collection ' + this.document);
};

/**
 * コネクション接続
 */

//TODO あとでDBの指定を変更
ModelModule.prototype.connection = function () {
	mongoose.connect('mongodb://localhost/sample_db');
};

/**
 * コネクション切断
 */
ModelModule.prototype.close = function () {
	mongoose.disconnect(function (err) {
		console.log(err ? err : 'connection close');
	});
};

/*---------------------------------------------
 * Model定義
 *--------------------------------------------*/
var Model = function (document, field) {
	this.document = document;
	this.field = field;
	this.scheme;
};

Model.prototype = new ModelModule();

/**
 * 更新用オブジェクト生成
 */
Model.prototype.getInstance = function () {
	return new this.scheme();
};

/**
 * SELECT
 */
Model.prototype.find = function (select, where, option) {
	var own = this;

	return new Promise(function (resolve, reject) {

		own.scheme.find(where || null, select || null, option || null, function (err, docs) {
			if (err) {
				return err;
			}
			result = [];

			docs.forEach(function (record) {
				result.push(record);
			});
			resolve(result);
		});
	});
};

Model.prototype.findOne = function (select) {
	var own = this.scheme

	return new Promise(function (resolve, reject) {
		own.findOne(select, function (err, doc) {
			if (err) {
				return err
			}
			resolve(doc)
		})
	})
}

/**
 * UPDATE
 */
Model.prototype.update = function (values, where, atomic) {
	this.scheme.update(where || {}, values, atomic || null, function (err) {
		console.log(err ? err : "update success");
	});
};

/**
 * DELETE
 */
Model.prototype.remove = function (where) {
	this.scheme.remove(where || null, function (err) {
		console.log(err ? err : "update success");
	});
};

/*---------------------------------------------
 * コンストラクタメソッド定義
 *--------------------------------------------*/
var ModelCreate = function(document,field){
    //モデルを生成し、インスタンスを返す
    model =  new Model(document,field);
    model.create();
    return model;
};

module.exports = ModelCreate;
