//TODO 全体　無駄なコメントは削除する!!
const io = require('socket.io')(60080);
const online = require('./module/Online');
const Mongo = require('./mongodb/WrapModel').useDB;
const Player = require('./module/userdata/Player');
let Schema = require('./mongodb/schema/SwitchSchema.js').SwitchSchema;
Schema = new Schema();

var roomList = {}
//ルームの初期化
for (var i = 0; i < 10000; i++) {
	var room = {
		roomId: i,
		level: 0,
		count: 0
	};
	roomList['room' + i] = room;
}

var mongo = new Mongo('player', Schema.getSchema('Player'));
mongo.connect()
console.log(mongo)

io.on('connection', function (socket) {
	socket['roomId'] = '';
	console.log('connect');

	socket.on('init', function (data) {
		//playerデータ(id)を受け取る
		console.log('run init');
		
		
		// mongo.find({_id: data._id}, function (result) {
		// 	var msg = {task: 'init', result: ''};
		//
		// 	var player;
		// 	if (result != null) {
		// 		player = new Player(result);
		// 		online('connectPlayer')(socket.id, player);
		// 	} else {
		// 		msg.result = 'failed'
		// 	}
		// 	socket.emit('result', msg)
		// });
	});


	socket.on('process', function (use) {
		var tmp = online(use.name)();
		console.log(tmp);

		//null,undefined,0などでない
		if (tmp) {
			socket.emit(use.name, tmp);
		}
		console.log(use.name + ' END');
	});

	socket.on('battle', function (json) {
		online('standByMatch')(socket.id)
	});

	socket.on('disconnect', function disconnect() {
		online('disconnectPlayer')(socket.id);
		console.log(socket.id + ':disconnected!!');
		mongo.close();
	});


	//TODO user登録
	socket.on('signUP', function (json) {
		// mongo.setSchema(Schema.getSchema('SignUp'));
		//
		// mongo.save(json, function (err) {
		// 	var message = {msg: ''};
		// 	if (err) {
		// 		message.msg = false
		// 	} else {
		// 		message.msg = true
		// 	}
		// 	//結果を返す
		// 	socket.emit('isSignIn', message);
		// });
	});

	socket.on('stateRegister', function (json) {

	})


	socket.on('test', function (json) {
		var msg = '';
		console.log(json);
		var result = true;
		if (!json) {
			result = false
		} else {
			for (var obj in json) {
				msg += obj + ":" + json[obj] + " "
			}
		}
		console.log(msg);
		socket.emit('get', {msg: msg, result: result})
	})
})
