/**
 * Created by bochan773 on 6/15/2016.
 */

// スキーマの作成
var schema = {
    'text':String,
    'game':String
};


//Modelを宣言
var TaskModel = require('./mongodb/model')('task', schema );


//コネクション接続
TaskModel.connection();

/*Delete
TaskModel.remove({'text': ''});
*/


//テストデータ
task = TaskModel.getInstance();
task.game = "finalfantasy";
task.text = "test1";


//Insert
task.save(function (err) {
    console.log(err ? err : 'insert success');
});


/*Update
TaskModel.update({'text': 'UPDATE!!'}, {'text': 'CHANGE'});
*/


//Select
var result = TaskModel.find('text').then(function (result) {
    Console.log(result);
});


