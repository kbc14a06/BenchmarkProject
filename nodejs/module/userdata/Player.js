"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
/// <reference path="User"/>
var User_1 = require("./User");
var Player = (function (_super) {
    __extends(Player, _super);
    function Player(json) {
        _super.call(this);
        //基本データ
        this.player = {
            id: '',
            name: '',
            level: 0,
            hp: 0,
            atk: 0,
            def: 0,
            agi: 0,
            exp: 0
        };
        _super.prototype.setData.call(this, this.player, json);
    }
    Player.prototype.getData = function () {
        return this.player.id;
    };
    return Player;
}(User_1.User));
exports.Player = Player;
//# sourceMappingURL=Player.js.map