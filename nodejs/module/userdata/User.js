"use strict";
var User = (function () {
    function User() {
    }
    User.prototype.setData = function (object, json) {
        for (var obj in json) {
            if (obj == '_id') {
                object['id'] = json[obj];
            }
            else {
                object[obj] = json[obj];
            }
        }
    };
    return User;
}());
exports.User = User;
//# sourceMappingURL=User.js.map