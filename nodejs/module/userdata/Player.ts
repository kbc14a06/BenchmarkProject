/// <reference path="User"/>
import {User} from "./User"

export class Player extends User {
	//基本データ
	private player = {
		id: '',
		name: '',
		level: 0,
		hp: 0,
		atk: 0,
		def: 0,
		agi: 0,
		exp: 0
	}

	constructor(json) {
		super();
		super.setData(this.player, json);
	}

	getData():Object {
		return this.player.id;
	}
}