var Memento = (function () {
    function Memento() {
        this.history = new Array();
    }
    Memento.prototype.add = function (history) {
        this.history.unshift(history);
    };
    return Memento;
}());
//# sourceMappingURL=Memento.js.map