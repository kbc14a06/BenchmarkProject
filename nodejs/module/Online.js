var processObj = {};
var players = {};

processObj.getMatchPlayerList = function () {
	var list = {};

	for (var id in players) {
		if (players[id].match == true) {
			list[id] = players[id];
		}
	}
	return list;
}

processObj.connectPlayer = function (id, player) {
	players[id] = player;
}


processObj.disconnectPlayer = function disconnectPlayer(id) {
	//接続ユーザーの削除を制限
	//disconnectの呼び出しのみ許可
	if ("disconnect" == disconnectPlayer.caller.name) {
		delete players[id];
	} else {
		console.log('Delete is prohibited');
	}
}

//マッチング待ち状態に変更
processObj.standByMatch = function (id) {
	
}


//レベル帯を指定する。
function levelZone(level) {
	//1の位のみ切り上げる
	var zone = Math.ceil( level / 10 ) * 10;
	return zone;
}


function process(processName) {
	return processObj[processName];
}

module.exports = process;