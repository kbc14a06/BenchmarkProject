class Battle {
	private player1;
	private player2;
	private turn = 1;
	
	constructor(player1, player2) {
		//playerオブジェクトの複製
		this.player1 = this.copyObject(player1);
		this.player2 = this.copyObject(player2);
	}

	setCommand(type,command) {
		switch(type) {
			case 1:
				this.player1.cmd = command;
				break;
			case 2:
				this.player2.cmd = command;
				break;
		}
	}
	
	runBattle() {
		this.turn++;
	}
	
	createHistory(): Battle {
		return <Battle>this.copyObject(this);
	}

	private copyObject(obj): Object {
		return JSON.parse(JSON.stringify(obj));
	}
}