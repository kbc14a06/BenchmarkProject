var Battle = (function () {
    function Battle(player1, player2) {
        this.turn = 1;
        //playerオブジェクトの複製
        this.player1 = this.copyObject(player1);
        this.player2 = this.copyObject(player2);
    }
    Battle.prototype.setCommand = function (type, command) {
        switch (type) {
            case 1:
                this.player1.cmd = command;
                break;
            case 2:
                this.player2.cmd = command;
                break;
        }
    };
    Battle.prototype.runBattle = function () {
        this.turn++;
    };
    Battle.prototype.createHistory = function () {
        return this.copyObject(this);
    };
    Battle.prototype.copyObject = function (obj) {
        return JSON.parse(JSON.stringify(obj));
    };
    return Battle;
}());
//# sourceMappingURL=Battle.js.map